Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2021-01-18T23:05:04+01:00

====== Pas assez de révolte ======


Nos représentants ont déjà du mal à gérer de petits problèmes, alors que les changements dont nous avons besoin sont bien plus profonds et globaux, par exemple :
* arrêter le [[https://fr.wikipedia.org/wiki/Dumping_social|dumping social]] et [[https://fr.wikipedia.org/wiki/Dumping_environnemental|environnemental]]
* établir la justice des revenus
* réorganiser la société pour qu'elle soit viable à long terme du point de vue écologique

Il faut donc d'autres types d'organisation que les partis politiques (centrés sur des leaders axés sur le paraître) pour réorganiser la société. Le travail à réaliser est grand et demande une structure adaptée.
